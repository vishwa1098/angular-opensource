import { Pipe, PipeTransform } from '@angular/core';



@Pipe({ name: 'angularEmployees' })
export class AngularEmployeesPipe implements PipeTransform {
    // Angularemployees: any[] = [];
  transform(allEmployees: any ) {
    return allEmployees.filter(employee => employee.knowsAngular);
  }
}

/////// Identical except for the pure flag
@Pipe({
  name: 'angularEmployeesImpure',
  pure: false
})
export class AngularEmployeesImpurePipe extends 
AngularEmployeesPipe {}
