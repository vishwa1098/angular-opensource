import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-angular-employee',
  templateUrl: './angular-employee.component.html'
})
export class AngularEmployeeComponent implements OnInit {
  employees: any[] = [];
  knowsAngular = true;
  mutate = true;
  constructor() { }

  ngOnInit(): void {
  }
  addEmployee(name: string) {
    name = name.trim();
    if (!name) { return; }
    let employee = { name, knowsAngular: this.knowsAngular };
    if (this.mutate) {
 
      this.employees.push(employee);
    } else {
   
      this.employees = this.employees.concat(employee);
    }
  }

}

@Component({
  selector: 'app-angular-employee-impure',
  templateUrl: './angular-employee-impure.component.html'

})
export class AngularEmployeeImpureComponent extends
  AngularEmployeeComponent {

}
