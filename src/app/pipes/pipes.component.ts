import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html'
})


export class PipesComponent implements OnInit {
  date = new Date(2020, 4, 15);
  toggle = true; 
  pi: number = 3.14;
  e: number = 2.718281828459045;
  collection: string[] = ['a', 'b', 'c', 'd'];
  constructor() { }

  ngOnInit(): void {
  }
  
  get format()   { return this.toggle ? 'shortDate' : 'fullDate'; }
  toggleFormat() { this.toggle = !this.toggle; }

}
