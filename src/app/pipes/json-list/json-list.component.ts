import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-json-list',
  template: `
  <h2>Employees from JSON File</h2>

  <div *ngFor="let employee of ('assets/employees.json' | fetch) ">
    {{employee.name}}
  </div>

  <p>Employees as JSON:
    {{'assets/employees.json' | fetch | json}}
  </p>`
})
export class JsonListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
