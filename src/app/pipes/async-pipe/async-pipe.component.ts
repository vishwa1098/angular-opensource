import { Component, OnInit } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-async-pipe',
  template: `
  <p>Message: {{ message$ | async }}</p>
  <button (click)="resend()">Resend</button>`,
})
export class AsyncPipeComponent implements OnInit {
  message$: Observable<string>;
  private messages = [
    'Message 1',
    'Message 2',
    'Message 3',
    'Message 4',
    'Message 5'
  ];

  constructor() { this.resend();}

  ngOnInit(): void {
  }
  resend() {
    this.message$ = interval(500).pipe(
      map(i => this.messages[i]),
      take(this.messages.length)
    );
}
}
