import { Component, OnInit } from '@angular/core';
import {} from './exponential.pipe';

@Component({
  selector: 'app-exponential-pipe',
  template: `
   {{2 | exponential: 10}}
  `
})
export class ExponentialPipeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
