import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async-promise',
  template: `<div>
  <code>promise|async</code>: 
  <button (click)="clicked()">{{ arrived ? 'Reset' : 'Resolve' }}</button>
  <span>Wait for it... {{ greeting | async }}</span>
</div>`
})
export class AsyncPromiseComponent implements OnInit {
  greeting: Promise<string>|null = null;
  arrived: boolean = false;

  private resolve: Function|null = null;

  constructor() {
    this.reset();
  }
  
  ngOnInit(): void {
  }

  reset() {
    this.arrived = false;
    this.greeting = new Promise<string>((resolve, reject) => {
      this.resolve = resolve;
    });
  }

  clicked() {
    if (this.arrived) {
      this.reset();
    } else {
      this.resolve!('hi there!');
      this.arrived = true;
    }
  }

}
