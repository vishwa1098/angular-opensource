import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exponentialcalc',
  template: `
  <div>
    <div>Number: <input [(ngModel)]="power"></div>
    <div>Power: <input [(ngModel)]="factor"></div>
    <p>
      Answer: {{power | exponential: factor}}
    </p>
    </div>
  `
})
export class ExponentialcalcComponent implements OnInit {
  power = 5;
  factor = 1;
  constructor() { }

  ngOnInit(): void {
  }

}
