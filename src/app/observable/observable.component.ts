import { Component, OnInit } from '@angular/core';
import { Student } from '../models/student.model';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {

  public students: Student[] = [];

  constructor(private studentservice: StudentService) {}

  ngOnInit() {
      const studentsObservable = this.studentservice.getStudents();
      studentsObservable.subscribe((studentsData: Student[]) => {
          this.students = studentsData;
      });
  }
  

}
