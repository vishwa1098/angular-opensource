import { Injectable } from '@angular/core';
import { Student } from '../models/student.model';
import { Observable } from 'rxjs';

 

@Injectable({
  providedIn: 'root'
})
export class StudentService {

students: Student[] = [{
    id: 1,
    name: 'Krunal',
    enrollmentnumber: 110470116021,
    college: 'DDIT',
},
{
    id: 2,
    name: 'Vishwa',
    enrollmentnumber: 110470116023,
    college: 'DDIT',
},
{
    id: 3,
    name: 'Ankit',
    enrollmentnumber: 110470116022,
    college: 'DDIT',
}];

  constructor() { }

  public getStudents(): any {
    const studentsObservable = new Observable(observer => {
           setTimeout(() => {
               observer.next(this.students);
           }, 1000);
    });

    return studentsObservable;
}

}
