import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import { Smartphone } from '../models/smartphone';
import { Observable } from 'rxjs';

// const localUrl = 'assets/smartphone.json';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url='https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=ddeefed6e7654ef69cf4317941c0fa10';

  constructor(private httpClient: HttpClient) {  }

  public getNews(){
    return this.httpClient.get(this.url);
  }

  
}
