import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ObservableComponent } from './observable/observable.component';
import { PipesComponent } from './pipes/pipes.component';
import { HttpClientExampleComponent } from './http-client-example/http-client-example.component';
import { BindingComponent } from './binding/binding.component';


const routes: Routes = [
  {
    path: 'observables',
    component: ObservableComponent
  },
  {
    path: 'pipes',
    component: PipesComponent
  },
  {
    path:'httpclient',
    component: HttpClientExampleComponent
  },
  {
    path:'bindings',
    component:BindingComponent
  }

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
