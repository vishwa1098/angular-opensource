import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ObservableComponent } from './observable/observable.component';
import { PipesComponent } from './pipes/pipes.component';
import { ExponentialPipeComponent } from './pipes/exponential-pipe.component';
import { ExponentialPipe} from './pipes/exponential.pipe';
import { ExponentialcalcComponent } from './pipes/exponentialcalc.component';
import { AngularEmployeeComponent, AngularEmployeeImpureComponent } from './pipes/angular-employee/angular-employee.component';
import {AngularEmployeesPipe, AngularEmployeesImpurePipe} from './pipes/angular-employee/angular-employee.pipe';
import { AsyncPipeComponent } from './pipes/async-pipe/async-pipe.component';
import { JsonListComponent } from './pipes/json-list/json-list.component';
import { FetchJsonPipe } from './pipes/json-list/fecth-json.pipe';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientExampleComponent } from './http-client-example/http-client-example.component';
import { ShadowDirective } from './binding/shadow.directive';
import { BindingComponent } from './binding/binding.component';
import { AsyncPromiseComponent } from './pipes/async-promise/async-promise.component';

@NgModule({
  declarations: [
    AppComponent,
    ObservableComponent,
    PipesComponent,
    ExponentialPipeComponent,
    ExponentialPipe,
    ExponentialcalcComponent,
    AngularEmployeeComponent,
    AngularEmployeeImpureComponent,
    AngularEmployeesPipe,
    AngularEmployeesImpurePipe,
    AsyncPipeComponent,
    JsonListComponent,
    FetchJsonPipe,
    HttpClientExampleComponent,
    ShadowDirective,
    BindingComponent,
    AsyncPromiseComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
